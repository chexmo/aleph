extends Control

@onready var timer : Timer = $Timer

@export var menu_scene : PackedScene

func _ready():
	timer.start(3)

func _on_timer_timeout():
	get_tree().change_scene_to_packed(menu_scene)
